package main

import (
	"flag"
	"fmt"
	"os"
	"os/exec"

	"github.com/vaughan0/go-ini"
)

var sourceDir string

func main() {
	// Парсим флаги
	flag.Parse()
	sourcefile := flag.Arg(0)
	if sourcefile == "" {
		fmt.Println("Flag false")
		// os.Exit(1)
	}

	// fmt.Println("Hello " + sourcefile + " >>True")

	file, err := ini.LoadFile("myfile.ini")
	if err != nil {
		fmt.Println("ini file not found")
		os.Exit(0)
	}

	backupDir, ok := file.Get("backup_doc", "backup_dir")
	if !ok {
		panic("'backup_dir' variable missing from 'backup_doc' section")
	}
	sourceDir, ok := file.Get("backup_doc", "source_dir")
	if !ok {
		panic("'source_dir' variable missing from 'backup_doc' section")
	}
	fmt.Println("======TEST TAR =======")
	runCommand("tar", "-cvvzf", backupDir+"/backup.tar.bz2", sourceDir)

}

//Функция запуска комманды в интерактивном режиме
func runCommand(cmdName string, arg ...string) {
	cmd := exec.Command(cmdName, arg...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	err := cmd.Run()
	if err != nil {
		fmt.Printf("Failed to start. %s\n", err.Error())
		os.Exit(1)
	}
}
